#!/usr/bin/bash
(   set $(lsblk -pn -o FSTYPE,KNAME,UUID | grep -E '^([^ ]*fat)|(ntfs)')
    while [ -n "${1:?}" ]; do  
	sudo mkdir -p /home/liveuser/"${3:?}"
        sudo mount "${2:?}"  /home/liveuser/"${3:?}"
    shift 3; done
) 2>/dev/null
