%packages
-adobe-source-*
-google-noto-cjk-fonts-common
-google-noto-sans-fonts
-google-noto-sans-jp-fonts
-google-noto-sans-kr-fonts
-google-noto-sans-lisu-fonts
-google-noto-sans-mandaic-fonts
-google-noto-sans-mandiac-fonts
-google-noto-sans-meetei-mayek-fonts
-google-noto-sans-mono-cjk-jp-fonts
-google-noto-sans-mono-cjk-kr-fonts
-google-noto-sans-nko-fonts
-google-noto-sans-sc-fonts
-google-noto-sans-sinhala-fonts
-google-noto-sans-tagalog-fonts
-google-noto-sans-tai-tham-fonts
-google-noto-sans-tai-viet-fonts
-google-noto-sans-tc-fonts
-google-noto-serif-jp-fonts
-google-noto-serif-kr-fonts
-google-noto-serif-sc-fonts
-google-noto-serif-tc-fonts
-google-noto-serif-tw-fonts
-lklug-fonts
-smc-fonts*
-smc-meera-fonts
-sil-*
-khmeros-*
-paratype-*
-naver-nanum-*
-thai-*
-julietaula-*
-stix-fonts
-tabish-eeyek-fonts
%end
