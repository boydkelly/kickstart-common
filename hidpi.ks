%post
cat >> /etc/skel/.Xdefaults <<FOE
Xft.dpi: 200
Xft.autohint: 0
Xft.lcdfilter:  lcddefault
Xft.hintstyle:  hintfull
Xft.hinting: 1
Xft.antialias: 1
Xft.rgba: rgb
FOE

cat >>  /etc/vconsole.conf << FOE
FONT=ter-v32n
FOE

%end

