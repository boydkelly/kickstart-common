
lang fr_FR.UTF-8 --addsupport en_US.UTF-8
keyboard --xlayouts=us,,ca,'ca (fr-legacy)' --switch=grp:alt_shift_toggle

#,ci,'ci (civ)','ci (civ_qw)'
#keyboard --layouts="fr-latin9","Côte d'Ivoire,(Qwerty)","Côte d'Ivoire, Multilingual","French(Côte d'Ivoire)"

%packages
glibc-minimal-langpack
glibc-langpack-en
glibc-langpack-fr
langpacks-fr 

%end

%post
cat >> /etc/locale.conf << FOE
LC_TIME='POSIX'
FOE

cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.input-sources.gschema.override << FOE
[org.gnome.desktop.input-sources]
show-all-sources=true
xkb-options=['grp:alt_shift_toggle', 'terminate:ctrl_alt_bksp', 'lv3:switch', 'numpad:microsoft']
per-window=false
current=uint32 0
mru-sources=[('xkb', 'ci+civ'), ('xkb', 'ci+civ_qw'), ('xkb', 'ci')]
sources=[('xkb', 'ci+civ'), ('xkb', 'ci+civ_qw'), ('xkb', 'ci')]
FOE

%end
