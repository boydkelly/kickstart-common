%packages
   -libvirt-*
	 -gnome-boxes
   -hyperv-daemons
   -open-vm-tools-desktop
   -virtualbox-guest-additions
   qemu-guest-agent
   spice-vdagent
	 #-xorg-x11-drv-*
	 #xorg-x11-drv-libinput
	 #xorg-x11-drv-intel
	 #xorg-x11-drv-fbdev
	 #xorg-x11-drv-wacom
	 #xorg-x11-drv-qxl
%end

%post
#Finally fix xorg.conf to work under kvm 
cat > /etc/X11/xorg.conf.kvm <<FOE
Section "ServerLayout"
	Identifier     "KVM xorg.conf"
	Screen      0  "Screen0" 0 0
	InputDevice    "Mouse0" "CorePointer"
	InputDevice    "Keyboard0" "CoreKeyboard"
EndSection

Section "Module"
	Load  "record"
	Load  "dri"
	Load  "extmod"
	Load  "glx"
	Load  "dbe"
	Load  "dri2"
EndSection

Section "InputDevice"
	Identifier  "Keyboard0"
	Driver      "kbd"
EndSection

Section "InputDevice"
	Identifier  "Mouse0"
	Driver      "vmmouse"
	Option	    "Protocol" "SysMouse"
	Option	    "Device" "/dev/sysmouse"
	Option	    "ZAxisMapping" "4 5 6 7"
EndSection

Section "Monitor"
	Identifier   "Monitor0"
	VendorName   "Monitor Vendor"
	ModelName    "Monitor Model"
	HorizSync       20.0 - 50.0
	VertRefresh     40.0 - 80.0
	Option          "DPMS"

EndSection

Section "Device"
	Identifier  "Card0"
	Driver      "vesa"
	VendorName  "KVM - std"
	BoardName   "GD 5446"
	BusID       "PCI:0:2:0"
EndSection

Section "Screen"
	Identifier "Screen0"
	Device     "Card0"
	Monitor    "Monitor0"
	SubSection "Display"
		Viewport   0 0
		Modes "3200x1800"
	EndSubSection
EndSection
FOE

%end
