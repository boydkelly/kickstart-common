
lang en_US.UTF-8 
#--addsupport en_CA.utf8
keyboard --xlayouts=us 


%packages
glibc-minimal-langpack
glibc-langpack-en
#glibc-all-langpacks
#glibc-langpack-fr
langpacks-en

%end

%post
cat >> /etc/locale.conf << FOE
LC_TIME='POSIX'
FOE

%end
