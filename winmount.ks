#mount windows partitions
%post
cat > /usr/local/bin/winmount.sh << 'FOE'
#!/usr/bin/bash
(   set \$(lsblk -pn -o FSTYPE,KNAME,UUID | grep -E '^([^ ]*fat)|(ntfs)')
    while [ -n "\${1:?}" ]; do  
	sudo mkdir -p /home/liveuser/"\${3:?}"
        sudo mount "\${2:?}"  /home/liveuser/"\${3:?}"
    shift 3; done
) 2>/dev/null    
FOE

chmod +x /usr/local/bin/winmount.sh && /usr/local/bin/winmount.sh

#automount windows partition if possible
mkdir -p /media/{DRIVEC,DRIVED,DRIVEE,WINDOWS}
echo "/dev/disk/by-label/DRIVEC /media/DRIVEC auto exec,user,uid=1000,gid=1000,dev,nofail,noauto,x-gvfs-show 0 0" >> /etc/fstab
echo "/dev/disk/by-label/DRIVED /media/DRIVED auto exec,user,uid=1000,gid=1000,dev,nofail,noauto,x-gvfs-show 0 0" >> /etc/fstab
echo "/dev/disk/by-label/DRIVEE /media/DRIVEE auto exec,user,uid=1000,gid=1000,dev,nofail,noauto,x-gvfs-show 0 0" >> /etc/fstab
echo "/dev/disk/by-label/WINDOWS /media/WINDOWS auto exec,user,uid=1000,gid=1000,dev,nofail,noauto,x-gvfs-show 0 0" >> /etc/fstab

%end
